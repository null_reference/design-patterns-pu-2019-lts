﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PowerUps
{
    [CreateAssetMenu(menuName = "Custom/Power Up Configuration")]
    public class PowerUpConfiguration : ScriptableObject
    {
        // Dictionary made to respect OpenClose SOLID Principle.
        [SerializeField] private PowerUp[] _powerUps;
        private Dictionary<string, PowerUp> _idToPowerUp;

        private void Awake()
        {
            _idToPowerUp = new Dictionary<string, PowerUp>();
            foreach (var powerUp in _powerUps)
            {
                _idToPowerUp.Add(powerUp.id, powerUp);
            }
        }

        public PowerUp GetPowerUpPrefabById(string id)
        {
            // Dictionary employed to respect OpenClose SOLID principle.
            PowerUp powerUp;
            if (!_idToPowerUp.TryGetValue(id, out powerUp))
            {
                throw new Exception($"PowerUp with id {id} does not exist");
            }

            return powerUp;
        }
    }
}
