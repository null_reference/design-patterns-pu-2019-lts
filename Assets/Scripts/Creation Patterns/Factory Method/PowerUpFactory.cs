﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PowerUps
{
    public class PowerUpFactory
    {
        // Less efficient way (but functional).
        /*
        [SerializeField] private PowerUp _speedPowerUp;
        [SerializeField] private PowerUp _drunkPowerUp;
        */

        private readonly PowerUpConfiguration _powerUpConfiguration;

        public PowerUpFactory(PowerUpConfiguration powerUpConfiguration)
        {
            _powerUpConfiguration = powerUpConfiguration;
        }

        public PowerUp Create(string id)
        {
            // Less efficient way (but functional).
            /*
            switch (id)
            {
                case "Speed":
                    return Instantiate(_speedPowerUp);
                case "Drunk":
                    return Instantiate(_drunkPowerUp);
                default:
                    throw new ArgumentOutOfRangeException($"PowerUp with Id {id} does not exist");
            }
            */

            var powerUp = _powerUpConfiguration.GetPowerUpPrefabById(id);
            return Object.Instantiate(powerUp);
        }
    }
}
