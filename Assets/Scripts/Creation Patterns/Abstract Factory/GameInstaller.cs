﻿using Heroes;
using Weapons;
using UnityEngine;

public class GameInstaller : MonoBehaviour
{
    [SerializeField] private HeroesConfiguration heroesConfiguration;
    [SerializeField] private WeaponsConfiguration weaponsConfiguration;
    [SerializeField] private WeaponsConfiguration magicEventWeaponsConfiguration;

    private Consumer consumer;
    private BattleFactory battleFactory;
    private BattleFactory magicEventBattleFactory;

    void Start()
    {
        var heroFactory = new HeroFactory(Instantiate(heroesConfiguration));
        var weaponFactory = new WeaponFactory(Instantiate(weaponsConfiguration));
        var magicEventWeaponFactory = new WeaponFactory(Instantiate(magicEventWeaponsConfiguration));

        var consumerGameObject = new GameObject();
        consumer = consumerGameObject.AddComponent<Consumer>();

        battleFactory = new BattleFactory(heroFactory, weaponFactory);
        magicEventBattleFactory = new BattleFactory(heroFactory, magicEventWeaponFactory);
        consumer.Configure(battleFactory);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("Magic Event activated !!!");
            consumer.Configure(magicEventBattleFactory);
        }
    }
}
